package br.com.entity;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "usuario", path = "usuario")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	List<User> findByUsername(@Param("username") String username);

}